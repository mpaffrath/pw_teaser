pw_teaser for TYPO3 CMS
=======================

Features
--------

* Create nested or flat views of your page structures
* extreme detailed options to filter pages available
* layout the teasers of your pages like you want with Fluid Templates
* pagination is also supported
* extendable with signals to modify or extend page models


Documentation
-------------

The documentation is outsourced to
https://forge.typo3.org/projects/extension-pw_teaser/wiki


Requirements
------------

Version 3.4 of pw_teaser requires:

* TYPO3 6.2 or higher


Links
-----

* **Donate:** https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=9LJYFQGJ7S232
* Issue Tracker: https://forge.typo3.org/projects/extension-pw_teaser/issues
* Source code: https://bitbucket.org/ArminVieweg/pw_teaser
